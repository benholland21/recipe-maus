// This file is for configuring axios to be used globally.
// Import axios from here in other files when you want to use it.

import axios from 'axios'

/*  Creating our Axios instance, with a baseURL.  */
var our_axios_instance = axios.create({
  baseURL: `http://${window.location.host}`,
})

// Add a request interceptor
our_axios_instance.interceptors.request.use(
  function (config) {
    return config
  },
  function (error) {
    console.warn("➡️ Error in Axios _request_:")
    console.error(error)
    return Promise.reject(error)
  }
)

// Add a response interceptor
our_axios_instance.interceptors.response.use(
  // Any status code that lie within the range of 2xx cause this function to trigger
  function (response) {
    return response
  },
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  function (error) {
    console.warn("➡️ Error in Axios _response_: ")
    console.error(error)
    return Promise.reject(error)
  }
)



export default our_axios_instance
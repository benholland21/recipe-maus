import axios from '../../api'

const state = () => ({
  ingredients: [],
})

const getters = {
  allIngredients(state) {
    return state.ingredients;
  },
}

const actions = {
  // get list of all ingredients
  getAllIngredients({ commit }) {
    axios({
      url: '/api/ingredients',
      method: 'get',
    })
    .then((result) => {
      var ingredients = result.data
      console.log("Just got these ingredients: ");
      console.log(ingredients);
      commit('setIngredients', ingredients)
    })
  },
  addNewIngredient({ commit }, payload) {
    console.log("Posting this data: ")
    for (var entry of payload.entries()) {
      console.log(entry);
    }
    axios({
      url: '/api/ingredients',
      method: 'post',
      data: payload
    })
      .then((result) => {
        console.log("Successful!")
        commit("addIngredient", result);
      })
  }
}

const mutations = {
  addIngredient(state, payload) {
    state.ingredients.push(payload)
  },
  setIngredients(state, payload) {
    state.ingredients = payload;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}

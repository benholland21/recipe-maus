import axios from '../../api'

const state = () => ({
  current_user: {},
  logged_in: false,
  session_id: '',
  users: [],
  auth_error: '',

  sessions: [],
})

const getters = {
  currentUser(state) {
    return state.current_user
  },
  allUsers(state) {
    return state.users;
  },
  authError(state) {
    return state.auth_error;
  },
  allSessions(state) {
    return state.sessions;
  }
}

const actions = {

  //  Register a new user.
  register({ commit }, payload) {
    console.log("Posting this data: ");
    var user_data = {};
    for (var entry of payload.entries()) {
      console.log(entry);
      user_data[entry[0]] = entry[1];
    }
    axios({
      url: '/api/register',
      method: 'post',
      data: payload
    })
    .then((result) => {
      console.log("Just regsitered a new user!: ");
      console.log(result.data)
      commit('setCurrentUser', {username: user_data.username});
      console.log(`Your current session id: ${result.data.sessionid}`)
      commit('setSessionId', result.data.sessionid)
      localStorage.setItem('sessionid', result.data.sessionid);
    })
  },

  //  Log user in
  login({ commit }, payload) {
    console.log("Logging in with: ");
    var user_data = {};
    for (var entry of payload.entries()) {
      console.log(entry);
      user_data[entry[0]] = entry[1];
    }
    axios({
      url: '/api/login',
      method: 'post',
      data: payload
    })
    .then((result) => {
      console.log(result)
      if (result.data.success) {
        console.log("Just logged in!: ");
        commit('setCurrentUser', {username: user_data.username});
        console.log(`Your current session id: ${result.data.session_id}`)
        localStorage.setItem('sessionid', result.data.sessionid);
      } else {
        console.log(`Incorrect username/password!`)
        commit('setAuthError', `Incorrect username/password!`)
      }
      
    })
  },

  //  Get all users
  getAllUsers({ commit }) {
    axios({
      url: '/api/get-users',
      method: 'get',
    })
    .then((result) => {
      console.log(`Getting all users:`)
      console.log(result)
      commit('setUsers', result.data)
    })
  },

  //  Get all session data
  getAllSessions({ commit }) {
    axios({
      url: '/api/get-sessions',
      method: 'get',
    })
    .then((result) => {
      console.log(`Getting all sessions:`)
      console.log(result)
      commit('setSessions', result.data)
    })
  },

  //  Get user by session ID
  getUserBySession({ commit }, payload) {
    axios({
      url: `/api/get-user-by-sessionid/${payload.sessionid}`,
      method: 'get',
    })
    .then((result) => {
      if (result) {
        console.log(`Found the user:`)
        console.log(result)
        commit('setCurrentUser', result.data)
      }
    })
  }

}

const mutations = {
  setCurrentUser(state, payload) {
    state.current_user = payload;
  },
  setUsers(state, payload) {
    state.users = payload;
  },
  setAuthError(state, payload) {
    state.auth_error = payload
  },
  setSessions(state, payload) {
    state.sessions = payload;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}

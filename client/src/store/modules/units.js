import axios from '../../api'

const state = () => ({
  units: [
    {
      name: 'tsp',
      aka: ['teaspoon', 't'],
    },
    {
      name: 'tbsp',
      aka: ['tablespoon', 'T'],
    },
    {
      name: 'fl oz',
      aka: ['fluid ounce']
    },
    {
      name: 'cup',
      aka: []
    },
    {
      name: 'pint',
      aka: []
    }
  ],
})

const getters = {
  allUnits(state) {
    return state.units;
  },
}

const actions = {
  // get list of all ingredients
  getAllUnits({ commit }) {
    axios({
      url: '/api/units',
      method: 'get',
    })
      .then((result) => {
        var units = result.data
        console.log("Just got these units: ");
        console.log(units);
        commit('setUnits', units)
      })
  },
}

const mutations = {
  setUnits(state, payload) {
    state.units = payload;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}

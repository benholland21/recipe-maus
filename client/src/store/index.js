import { createStore } from 'vuex'
import ingredients from './modules/ingredients'
import units from './modules/units'
import users from './modules/users'


export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    ingredients,
    units,
    users,
  }
})

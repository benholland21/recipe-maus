import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../views/Dashboard/index.vue'

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  // Auth
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/User/Register.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/User/Login.vue')
  },
  //  Grocery list
  {
    path: '/grocery-list',
    name: 'GroceryList',
    component: () => import('../views/GroceryList/index.vue')
  },
  //  Meal schedule
  {
    path: '/meal-schedule',
    name: 'MealSchedule',
    component: () => import('../views/MealSchedule/index.vue')
  },
  //  Recipes
  {
    path: '/recipes',
    name: 'Recipes',
    component: () => import('../views/Recipes/index.vue')
  },
  {
    path: '/recipes/new',
    name: 'NewRecipe',
    component: () => import('../views/Recipes/NewRecipe.vue')
  },
  //  Ingredientpedia
  {
    path: '/ingredientpedia',
    name: 'ingredientpedia',
    component: () => import('../views/Ingredientpedia/index.vue')
  },
  //  Admin page
  {
    path: '/admin',
    name: 'adminpage',
    component: () => import('../views/Admin.vue')
  },
  //  404
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('../views/NotFound.vue')
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

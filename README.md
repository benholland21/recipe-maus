# Recipe Maus

An app for tracking recipes, scheduling meals, and planning groceries. 

<br/><br/><br/><br/>



## Dev set up

### A. Cloning the repo

To clone this code from Gitlab onto your computer, you'll need to create & register a private ssh key. 
These instructions are adapted from https://docs.gitlab.com/ee/ssh/

1. Run `cd ~/.ssh/` in a terminal.  This is where we'll put the key.
1. Run `ssh-keygen -t ed25519 -C "<your-email>"` to create your key. 
1. Press ENTER through the prompts. 
1. Run `ls`.  You should see a file called `id_ed25519.pub`, that's your public key. 
1. Copy the contents of the `id_ed25519.pub` file, either by opening it in a text editor or using a script.
 - A script for mac:  `tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy`
 - A script for Windows: `cat ~/.ssh/id_ed25519.pub | clip`
1. Open a browser to gitlab.com and sign in. 
1. On the left side bar, select "SSH keys"
1. Paste in your key contents, name the computer the key corresponds to, and hit "save".

### B. Installing NodeJS

1. Just look it up

### C. Setting up the project

1. In `/recipe-maus/`, run `npm install`


<br/><br/><br/><br/>

## Dev workflow

1. In `/recipe-maus/`, start the dev server by running `node ./server/app.js`
1. Open a new terminal while keeping the server running.
1. In `/recipe-maus/client/`, start the hot-reloading client server by running `npm run serve`


<br/><br/><br/><br/>

## Resources

 - [FormData](https://developer.mozilla.org/en-US/docs/Web/API/FormData)
 - [ExpressJS](https://expressjs.com/en/starter/installing.html)
 - [pg-promise](https://expressjs.com/en/guide/database-integration.html#postgresql)
 - [Multer](npmjs.com/package/multer#readme)
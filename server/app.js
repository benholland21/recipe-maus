
//  ExpressJS tools
const express = require('express')
const app = express()
const port = 8000

//  Multer setup
const multer  = require('multer')
const upload = multer({ dest: __dirname + '/uploads' })

//  Postgres tools
var pgp = require('pg-promise')(/* options */)
var db = pgp('postgres://benholland:WaterHavoc33y@localhost:5432/recipe-maus')


//  Adding routes
require('./routes/ingredient-routes.js')(app, db, upload);
require('./routes/auth-routes.js')(app, db, upload);



app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})




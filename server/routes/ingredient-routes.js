
module.exports = function(app, db, upload) {

  //  Getting ingredients from the db.
  app.get('/api/ingredients', (req, res) => {

    db.any('SELECT * FROM ingredients')
    .then(function (data) {
      res.send(data);
    })
    .catch(function (error) {
      console.log('ERROR:', error);
      res.send("ERROR: " + error);
    })

  })

  //  Adding an ingredient to the db.
  app.post('/api/ingredients', upload.single('ingredient-img'), (req, res) => {
    console.log("New ingredient posted to /api/ingredient");
    console.log(req.file)
    console.log(req.body);
    // db.none('INSERT INTO ingredients(name, img, units, created_on) VALUES (${name}, ${img}, ${units}, ${created_on})', {
    //   name: req.body.name,
    //   img: req.body.img,
    //   units: req.body.units,
    //   created_on: req.body.created_on
    // })
    // .then(function (data) {
    //   res.send(data);
    // })
    // .catch(function (error) {
    //   console.log('ERROR:', error);
    //   res.send("ERROR: " + error);
    // })

  })

}


